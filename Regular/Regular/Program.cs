﻿﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Regular
{
	class Program
	{
		static void Main(string[] args)
		{
			StringBuilder sb = new StringBuilder();
			string currentLine = string.Empty;
			Regex regExp = new Regex(@"(xln[^\d]*)", RegexOptions.IgnoreCase);
			MatchCollection collection;
			using (var textReader = System.IO.File.OpenText(@"c:\results.txt"))
			{
				while (!textReader.EndOfStream)
				{
					currentLine = textReader.ReadLine();
					sb.Append(currentLine);
					collection = regExp.Matches(currentLine);
					if (collection?.Count>0)
					{
						sb.Append('\t');
						sb.Append(collection[0].Groups[0].Value);
					}
					sb.AppendLine();
					//test
				}
				textReader.Close();
			}
			using (var textWrite = System.IO.File.Open(string.Format(@"c:\results_parsed{0}.txt", System.DateTime.Now.ToString("ddmmyyyhh24miSSSS")), System.IO.FileMode.Create))
			{
				byte[] byteArray = System.Text.Encoding.UTF8.GetBytes(sb.ToString());
				textWrite.Write(byteArray, 0, byteArray.Length);
				textWrite.Close();
			}
		}
	}
}
